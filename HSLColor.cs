using System;
using System.Drawing;

namespace ColorBender
{
	public struct HSVColor
	{
		private double hue;
		public double Hue
		{
			get => hue;
			set => hue = value % 360;
		}

		private double saturation;
		public double Saturation
		{
			get => saturation;
			set => saturation = Math.Clamp(value, 0, 1);
		}

		private double value;
		public double Value
		{
			get => value;
			set => this.value = Math.Clamp(value, 0, 1);
		}

		private double alpha;
		public double Alpha
		{
			get => alpha;
			set => alpha = Math.Clamp(value, 0, 1);
		}

		public static HSVColor FromARGB(Color argb)
		{
			// normalize red, green and blue values
			double r = argb.R / 255.0;
			double g = argb.G / 255.0;
			double b = argb.B / 255.0;

			double max = Math.Max(r, Math.Max(g, b));
			double min = Math.Min(r, Math.Min(g, b));
			var diff = max - min;

			double h = 0.0;
			if (max == r && g >= b)
			{
				h = 60 * (g - b) / diff;
			}
			else if (max == r && g < b)
			{
				h = 60 * (g - b) / diff + 360;
			}
			else if (max == g)
			{
				h = 60 * (b - r) / diff + 120;
			}
			else if (max == b)
			{
				h = 60 * (r - g) / diff + 240;
			}

			if (double.IsNaN(h))
				h = 0;

			double s = (max == 0) ? 0.0 : (1.0 - (min / max));

			return new HSVColor
			{
				alpha = argb.A / 255.0,
				hue = h,
				saturation = s,
				value = max
			};
		}

		public Color ToRGB()
		{
			double hueSmall = hue / 60;
			int hueRatchet = (int)(hueSmall % 6);
			double hueRemainder = hueSmall - hueRatchet;
			double p = value * (1 - saturation);
			double q = value * (1 - hueRemainder * saturation);
			double t = value * (1 - (1 - hueRemainder) * saturation);

			double r = 0;
			double g = 0;
			double b = 0;
			switch (hueRatchet)
			{
				case 0:
					r = value; g = t; b = p; break;
				case 1:
					r = q; g = value; b = p; break;
				case 2:
					r = p; g = value; b = t; break;
				case 3:
					r = p; g = q; b = value; break;
				case 4:
					r = t; g = p; b = value; break;
				case 5:
					r = value; g = p; b = q; break;
			}
			return Color.FromArgb(
				(int)(alpha * 255),
				(int)(r * 255),
				(int)(g * 255),
				(int)(b * 255)
			);
		}
	}
}