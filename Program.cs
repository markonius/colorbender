﻿using System;
using System.CommandLine;
using System.CommandLine.Parsing;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using Victor.Yves;

namespace ColorBender
{
	record Options
	(
		double? TargetHue,
		double TargetHueWidth,
		double Hue,
		double Saturation,
		double Value
	);

	static class Program
	{
		static void Main(string[] args)
		{
			Options? options = ParseArgs(args);
			if (options != null)
				Process(Console.In, Console.Out, options);
			else
				Environment.Exit(1);
		}

		static Options? ParseArgs(string[] args)
		{
			RootCommand rootCommand = new RootCommand($"{nameof(ColorBender)} - modify colors in text files");

			Option<double?> targetHue = new Option<double?>(
				aliases: new[] { $"--{nameof(targetHue)}", "-t" },
				description: "If specified, only this hue will be affected.",
				getDefaultValue: () => null
			)
			{
				Name = nameof(targetHue),
				IsRequired = false
			};
			rootCommand.AddOption(targetHue);

			Option<double> targetHueWidth = new Option<double>(
				aliases: new[] { $"--{nameof(targetHueWidth)}", "-w" },
				description: $"If specified, sets the range of hues around ${nameof(targetHue)} to be affected.",
				getDefaultValue: () => 0
			)
			{
				Name = nameof(targetHueWidth),
				IsRequired = false
			};
			rootCommand.AddOption(targetHueWidth);

			Option<double> hue = new Option<double>(
				aliases: new[] { $"--{nameof(hue)}", "-h" },
				description: $"Hue change.",
				getDefaultValue: () => 0
			)
			{
				Name = nameof(hue),
				IsRequired = false
			};
			rootCommand.AddOption(hue);

			Option<double> saturation = new Option<double>(
				aliases: new[] { $"--{nameof(saturation)}", "-s" },
				description: $"Saturation change.",
				getDefaultValue: () => 0
			)
			{
				Name = nameof(saturation),
				IsRequired = false
			};
			rootCommand.AddOption(saturation);

			Option<double> value = new Option<double>(
				aliases: new[] { $"--{nameof(value)}", "-v" },
				description: $"Value change.",
				getDefaultValue: () => 0
			)
			{
				Name = nameof(value),
				IsRequired = false
			};
			rootCommand.AddOption(value);

			ParseResult parseResult = rootCommand.Parse(args);
			if (parseResult.UnmatchedTokens.Any())
			{
				foreach (string token in parseResult.UnmatchedTokens)
					Console.Error.WriteLine($"Unrecognized argument \"{token}\"");
				return null;
			}
			else
			{
				return new Options(
					parseResult.ValueForOption(targetHue),
					parseResult.ValueForOption(targetHueWidth),
					parseResult.ValueForOption(hue),
					parseResult.ValueForOption(saturation),
					parseResult.ValueForOption(value)
				);
			}
		}

		static void Process(TextReader input, TextWriter output, Options options)
		{
			while (true)
			{
				int c = input.Read();
				if (c < 0)
					break;

				if (c == '#')
				{
					Either<ColorString, string> colorString = ExtractColorString(input);
					colorString.Match
					(
						colorString =>
						{
							Color color = ColorTranslator.FromHtml("#" + colorString.RGB);
							var hsvColor = HSVColor.FromARGB(color);

							if (options.TargetHue == null ||
								IsHueInRange(hsvColor.Hue, (double)options.TargetHue, options.TargetHueWidth))
							{
								hsvColor.Hue += options.Hue;
								hsvColor.Saturation += options.Saturation;
								hsvColor.Value += options.Value;

								color = hsvColor.ToRGB();
								string outputString = ColorToString(color) + colorString.Alpha;
								output.Write(outputString);
							}
							else
							{
								output.Write('#' + colorString.RGB + colorString.Alpha);
							}
						},
						s =>
						{
							output.Write(s);
						}
					);
				}
				else
				{
					output.Write((char)c);
				}
			}
		}

		static bool IsHueInRange(double hue, double rangeCenter, double rangeWidth)
		{
			double lowerHue = hue - 360;
			double upperHue = hue + 360;
			double halfRange = rangeWidth / 2;
			double min = rangeCenter - halfRange;
			double max = rangeCenter + halfRange;

			return (hue > min && hue < max) ||
				(lowerHue > min && lowerHue < max) ||
				(upperHue > min && upperHue < max);
		}

		public static string ColorToString(Color color) => $"#{color.R:X2}{color.G:X2}{color.B:X2}";

		static Either<ColorString, string> ExtractColorString(TextReader input)
		{
			var stringBuilder = new StringBuilder(6);

			for (int i = 0; i < 6; i++)
			{
				int read = input.Read();
				if (read < 0)
					return stringBuilder.ToString();
				char c = (char)read;

				stringBuilder.Append(c);
				if (!IsHexDigit(c))
					return stringBuilder.ToString();
			}

			int peeked1 = input.Peek();
			if (peeked1 == '#' || peeked1 < 0 || !IsHexDigit((char)peeked1))
				return new ColorString(stringBuilder.ToString(), string.Empty);
			char extra1 = (char)input.Read();

			int peeked2 = input.Peek();
			if (peeked2 < 0 || !IsHexDigit((char)peeked2))
			{
				stringBuilder.Append(extra1);
				return stringBuilder.ToString();
			}
			char extra2 = (char)input.Read();

			int peeked3 = input.Peek();
			if (peeked3 < 0 || !IsHexDigit((char)peeked3))
				return new ColorString(stringBuilder.ToString(), "" + extra1 + extra2);

			stringBuilder.Append(extra1);
			stringBuilder.Append(extra2);
			return stringBuilder.ToString();
		}

		static bool IsHexDigit(char c)
		{
			return char.IsDigit(c) || (c >= 65 && c <= 70) || (c >= 97 && c <= 102);
		}
	}

	record ColorString
	(
		string RGB,
		string Alpha
	);
}
